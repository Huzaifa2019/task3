package com.example.task3;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public void listen(String val) {
        Toast a = Toast.makeText(MainActivity.this,val,Toast.LENGTH_LONG);
        a.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listen("Created");
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        listen("Started");
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
        listen("Paused");
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        listen("Resumed");
        setContentView(R.layout.activity_main);
    }

    @Override
    public void finish() {
        super.finish();
        listen("Finished");
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        listen("Destroyed");
        setContentView(R.layout.activity_main);

    }

}